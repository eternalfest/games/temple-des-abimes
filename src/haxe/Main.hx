import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import maxmods.Misc;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	gameParams: GameParams,
	noNextLevel: NoNextLevel,
    misc: Misc,
	merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
